package com.varthana.studentloan.CoApplicantEmploymentDetail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoApplicantEmploymentService {

    @Autowired
    CoApplicantEmploymentRepository repository;

    public List<CoApplicantEmployment> getAllCoApplicant() {
        List<CoApplicantEmployment> coApplicants = new ArrayList<>();
        repository.findAll().forEach(coApplicants::add);
        return coApplicants;
    }

    public void addCoApplicantEmployment(CoApplicantEmployment coApplicant) {
        repository.save(coApplicant);
    }

    public Optional<CoApplicantEmployment> getCoApplicantById(Integer id) {
		return repository.findById(id);
	}

}