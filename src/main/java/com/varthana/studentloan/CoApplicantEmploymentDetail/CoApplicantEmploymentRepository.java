package com.varthana.studentloan.CoApplicantEmploymentDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

// import java.util.List;

// import org.springframework.data.repository.Repository;

public interface CoApplicantEmploymentRepository extends CrudRepository<CoApplicantEmployment, Integer> {
   
}