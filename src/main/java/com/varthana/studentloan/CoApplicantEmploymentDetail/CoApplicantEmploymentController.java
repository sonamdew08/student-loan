package com.varthana.studentloan.CoApplicantEmploymentDetail;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080" })
public class CoApplicantEmploymentController {

    @Autowired
    private CoApplicantEmploymentService coApplicantEmploymentService;

    Logger logger = LoggerFactory.getLogger(CoApplicantEmploymentController.class);

    @RequestMapping("/coApplicant")
    public List<CoApplicantEmployment> getAllCoApplicant() {
        return coApplicantEmploymentService.getAllCoApplicant();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/coApplicant")
    public void addCoApplicantEmployment(@RequestBody CoApplicantEmployment coApplicant) {
        logger.info("------------CoApplicant-----------");
        logger.info(coApplicant.getUser_info_id().toString());
        logger.info(coApplicant.getName_of_employer_or_business());
        coApplicantEmploymentService.addCoApplicantEmployment(coApplicant);
    }

    @RequestMapping("/coApplicant/{id}")
    public Optional<CoApplicantEmployment> getCoApplicantById(@PathVariable Integer id) {
        return coApplicantEmploymentService.getCoApplicantById(id);
    }
}