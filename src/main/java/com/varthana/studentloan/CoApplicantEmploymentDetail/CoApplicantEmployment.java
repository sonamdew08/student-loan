package com.varthana.studentloan.CoApplicantEmploymentDetail;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"created_on"}, 
        allowGetters = true)
@Table(name="co_applicant_employment_detail")
public class CoApplicantEmployment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer co_applicant_id;

    @Column(name = "occupation")
    public String occupation;

    @Column(name = "name_of_employer_or_business")
    public String name_of_employer_or_business;

    @Column(name = "address")
    public String address;

    @Column(name = "city")
    public String city;

    @Column(name = "state")
    public String state;

    @Column(name = "pincode")
    public String pincode;

    @Column(name = "designation")
    public String designation;

    @Column(name = "monthly_salary")
    public String monthly_salary;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    public Date created_on;

    @Column(name = "user_info_id")
    public Integer user_info_id;

	public Integer getCo_applicant_id() {
		return this.co_applicant_id;
	}

	public void setCo_applicant_id(Integer co_applicant_id) {
		this.co_applicant_id = co_applicant_id;
	}

	public String getOccupation() {
		return this.occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getName_of_employer_or_business() {
		return this.name_of_employer_or_business;
	}

	public void setName_of_employer_or_business(String name_of_employer_or_business) {
		this.name_of_employer_or_business = name_of_employer_or_business;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getMonthly_salary() {
		return this.monthly_salary;
	}

	public void setMonthly_salary(String monthly_salary) {
		this.monthly_salary = monthly_salary;
	}

	public Date getCreated_on() {
		return this.created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}	
	
	public Integer getUser_info_id() {
		return this.user_info_id;
	}

	public void setUser_info_id(Integer user_info_id) {
		this.user_info_id = user_info_id;
	}

    // CoApplicantEmployment(){

    // }


}