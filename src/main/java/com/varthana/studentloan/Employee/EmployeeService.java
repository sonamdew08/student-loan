package com.varthana.studentloan.Employee;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
// import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService{

    @Autowired 
    EmployeeRepository repository;

    Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    public List<Employee> getAllUser(){
        List<Employee> users = new ArrayList<>();
        repository.findAll()
        .forEach(users::add);
        return users;
    }

	public void addEmployee(Employee employee) {
        logger.info("----------employee controller---------");
        logger.info(employee.toString());
        
        repository.save(employee);
	}
}