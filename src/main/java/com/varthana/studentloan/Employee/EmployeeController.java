package com.varthana.studentloan.Employee;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080"})
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @RequestMapping("/employee")
    public List<Employee> getAllUser(){
        return employeeService.getAllUser();
    }

    @RequestMapping(method = RequestMethod.POST, value="/employee")
    public ResponseEntity addEmployee(@RequestBody Employee employee){
        logger.info("----------employee controller---------");
        logger.info(employee.getEmail());
        logger.info(employee.getName());
        logger.info(employee.getPassword());
        employeeService.addEmployee(employee);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
    