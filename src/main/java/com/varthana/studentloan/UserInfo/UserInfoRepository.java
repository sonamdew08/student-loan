package com.varthana.studentloan.UserInfo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface UserInfoRepository extends CrudRepository<UserInfo, Integer> {
   
}