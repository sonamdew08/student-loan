package com.varthana.studentloan.UserInfo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.varthana.studentloan.User.User;


@Entity
@Table(name="user_info", schema = "public")
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer userinfo_id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    public User user_id;

    @Column(name = "gender")
    public String gender;

    @Column(name = "date_of_birth")
    public String date_of_birth;

    @Column(name = "maritalstatus")
    public String maritalstatus;

    @Column(name = "name")
    public String name;

    @Column(name = "father_name")
    public String father_name;

    @Column(name = "mother_name")
    public String mother_name;

    @Column(name = "email")
    public String email;

    public UserInfo() {
        
    }

	public Integer getUserinfo_id() {
		return this.userinfo_id;
	}

	public void setUserinfo_id(Integer userinfo_id) {
		this.userinfo_id = userinfo_id;
    }
    
	public User getUser_id() {
		return this.user_id;
	}

	public void setUser_id(User user_id) {
		this.user_id = user_id;
    }
    
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDate_of_birth() {
		return this.date_of_birth;
	}

	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getMaritalstatus() {
		return this.maritalstatus;
	}

	public void setMaritalstatus(String maritalstatus) {
		this.maritalstatus = maritalstatus;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFather_name() {
		return this.father_name;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public String getMother_name() {
		return this.mother_name;
	}

	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
    }
    
    

}