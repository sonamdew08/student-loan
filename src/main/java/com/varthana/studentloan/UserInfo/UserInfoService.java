package com.varthana.studentloan.UserInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService {

    @Autowired
    UserInfoRepository repository;

    Logger logger = LoggerFactory.getLogger(UserInfoService.class);

    public List<UserInfo> getAllUser() {
        List<UserInfo> users = new ArrayList<>();
        repository.findAll().forEach(users::add);
        return users;
    }

    public void updateUserDetail(Integer userinfo_id, UserInfo userinfo) {
        Optional<UserInfo> user = repository.findById(userinfo_id);
        logger.info("======ouside ispresent========");
        if(user.isPresent()){
            logger.info("======inside ispresent========");
            userinfo.setUserinfo_id(userinfo_id);
            repository.save(userinfo);
        }      
    }   

    public Optional<UserInfo> getUserDetail(Integer userinfo_id) {
        return repository.findById(userinfo_id);
        
	}
}