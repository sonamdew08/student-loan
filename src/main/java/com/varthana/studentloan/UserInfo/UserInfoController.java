package com.varthana.studentloan.UserInfo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @RequestMapping("/userinfo")
    public List<UserInfo> getAllUser() {
        return userInfoService.getAllUser();
    }

    @RequestMapping("/userinfo/{id}")
    public Optional<UserInfo> getUserDetail(@PathVariable(value = "id") Integer userinfo_id) {
        return userInfoService.getUserDetail(userinfo_id);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/userinfo/{id}")
    public void updateUserDetail(@PathVariable(value = "id") Integer userinfo_id,
            @RequestBody UserInfo userinfo) {
        userInfoService.updateUserDetail(userinfo_id, userinfo);
    }
}
    