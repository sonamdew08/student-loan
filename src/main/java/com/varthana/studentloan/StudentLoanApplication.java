package com.varthana.studentloan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication(scanBasePackages = {"com.varthana.studentloan"})
@EnableJpaAuditing
public class StudentLoanApplication {
    public static void main(String[] args) {
        SpringApplication.run(StudentLoanApplication.class, args);        
    }
}
