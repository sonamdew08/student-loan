package com.varthana.studentloan.User;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService{

    @Autowired 
    UserRepository repository;

    public List<User> getAllUser(){
        List<User> users = new ArrayList<>();
        repository.findAll()
        .forEach(users::add);
        return users;
    }
}