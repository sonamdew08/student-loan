package com.varthana.studentloan.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController{

    @Autowired
    private UserService userService;

    @RequestMapping("/users")
    public List<User> getAllUser(){
        return userService.getAllUser();
    }
}
    